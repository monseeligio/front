import { Injectable } from '@angular/core';
import { URL } from './config/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public url;

  constructor(private _http:HttpClient) {
    this.url = URL;
  }

  login(body : any) {
    const url = `${this.url}/login`;
    return this._http.post(url, body);
  }
  
  agregarConstalaci(body:any){
    const url= `${this.url}/new/constancia`;
    return this._http.post(url,body);
  }
  
  agregarRecono(body:any){
    const url= `${this.url}/new/reconocimiento`;
    return this._http.post(url,body);
  }
    
  consultaConstancia(){
    //variable propia del metodo
    const url= `${this.url}/obtener/datos/constancia`;
    return this._http.get(url);
  }
  
  consultareco(){
    //variable propia del metodo
    const url= `${this.url}/obtener/datos/reconocimientos`;
    return this._http.get(url);
  }

  eliminarUsuario(id:any){
    const url= `${this.url}/delete/constancia/${id}`;
    return this._http.delete(url);
  }

  obtenerUsers(){
    const url= `${this.url}/obtener/datos/usuarios`;
    return this._http.get(url);
  }

  agregarUser(body:any){
    const url= `${this.url}/new/user`;
    return this._http.post(url,body);
  }
  
  eliminarReconocimiento(id:any){
    const url= `${this.url}/delete/registro/${id}`;
    return this._http.delete(url);
  }
  
}
