import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { MenuComponent } from './components/menu/menu.component';
import { ConsultarComponent } from './components/consultar/consultar.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CrearusuarioComponent } from './components/crearusuario/crearusuario.component';
import { LoginComponent } from './components/login/login/login.component';
import { MenuNormalComponent } from './components/menu-normal/menu-normal.component';
import { ConsultaRComponent } from './components/consulta-r/consulta-r.component';

export const ROUTES: Routes = [
  {path:'login',component: LoginComponent},
  {path:'menuNormal',component: MenuNormalComponent},
  {path:'menu',component: MenuComponent},
  {path:'consultar',component: ConsultarComponent},
  {path:'consultarR',component: ConsultaRComponent},
  {path:'usuarios',component: UsuariosComponent},
  {path: 'crearusuario', component: CrearusuarioComponent},
  {path: '', pathMatch:'full' ,redirectTo:'login'},
  //{path:'**', pathMatch:'full',redirectTo:'home'}
];
