import { Component, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditarusuarioComponent } from '../editarusuario/editarusuario.component';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  public datos2:any[];

  constructor(private dialog: MatDialog, private _serviceM:ApiService) { }
  openDialog() {
    this.dialog.open(EditarusuarioComponent, {
      width: '30%'
    });
  }
  
  ngOnInit(): void {
    this.getUser()
  }

  getUser(){
    this._serviceM.obtenerUsers().subscribe((resp:any)=>{
      if (resp['ok']){
        console.log(resp);
        this.datos2=resp['respuesta'];
        console.log(this.datos2);
      }
    })
  }
}
