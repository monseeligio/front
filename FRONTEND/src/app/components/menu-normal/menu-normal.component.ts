import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-normal',
  templateUrl: './menu-normal.component.html',
  styleUrls: ['./menu-normal.component.css']
})
export class MenuNormalComponent implements OnInit {

  constancia=false;
  reconocimiento=false;

  constructor() { }

  ngOnInit(): void {

  }
  constancia2(){
    this.constancia=true;
    this.reconocimiento=false;
  }
  reconocimiento2(){
    this.constancia=false;
    this.reconocimiento=true;
  }
}
