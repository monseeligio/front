import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuNormalComponent } from './menu-normal.component';

describe('MenuNormalComponent', () => {
  let component: MenuNormalComponent;
  let fixture: ComponentFixture<MenuNormalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuNormalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuNormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
