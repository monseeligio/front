import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';
import * as moment from 'moment';


import { FormBuilder, FormControlName } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import html2canvas from 'html2canvas';
import {
  NgxQrcodeElementTypes,
  NgxQrcodeErrorCorrectionLevels
} from "@techiediaries/ngx-qrcode";

@Component({
  selector: 'app-constancia',
  templateUrl: './constancia.component.html',
  styleUrls: ['./constancia.component.css']
  
})
export class ConstanciaComponent implements OnInit {
  formulario: any;
  Nombre='';
  Rol='';
  NombreP:'';
  Clave='';
  FechaI='';
  FechaC='';
  FechaE='';
  Horas='';
  public btnAgregar = false;
  public datos:any;
  Datos={Nombre:'', Rol:'', NombreP:'', Clave:'', FechaI:'', FechaC:'', FechaE:'', Horas:''}

  tipoElemento = NgxQrcodeElementTypes.IMG;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  valor: any;


  constructor(private fb: FormBuilder, private _serviceM:ApiService) { }
 
  ngOnInit(): void {
    this.crearFormulario();

  }
  
    get experienciaLaboral(): FormArray {
      return this.formulario.get('experienciaLaboral') as FormArray;    
  }
  get valores() {
    return this.formulario['controls'].experienciaLaboral['controls'] as FormArray;
   }

  crearFormulario() {
      this.formulario = this.fb.group({
      experienciaLaboral: this.fb.array([
      ]),
      NombreP: ['', Validators.required],
      clave: ['', Validators.required],
      FechaI: ['', Validators.required],
      FechaC: ['', Validators.required],
      FechaE:['', Validators.required],
      Horas: ['', Validators.required]
    });
    this.anadirExperienciaLaboral()
  }
  
  anadirExperienciaLaboral() {       
    const trabajo = this.fb.group({
      Nombre: ['', Validators.required],
      Rol: ['', Validators.required]
    });
    this.experienciaLaboral.push(trabajo); 
   
    }

  anadirExperienciaLaboral2() {       
  const trabajo = this.fb.group({
    Nombre: ['', Validators.required],
    Rol:['', Validators.required]
  });
  this.formulario['controls']['experienciaLaboral'].push(trabajo); 
  }

borrarTrabajo(indice: number) {
  if( this.experienciaLaboral.length>1){
    this.experienciaLaboral.removeAt(indice);
  }
}


agregarConstancia() {
      if (this.formulario.valid) {
        for (let index = 0; index < this.experienciaLaboral.length; index++) {
          this.valor=[this.Nombre=this.experienciaLaboral.value[index]['Nombre'], this.experienciaLaboral.value[index]['Rol'], this.Clave=this.formulario.get('clave').value, this.formulario.get('NombreP').value ]
            this.Datos.Nombre=this.experienciaLaboral.value[index]['Nombre'];
            this.Datos.Rol=this.experienciaLaboral.value[index]['Rol'];
            this.Datos.NombreP=this.formulario.get('NombreP').value
            this.Datos.Clave=this.formulario.get('clave').value
            this.Datos.FechaI=this.formulario.get('FechaI').value
            this.Datos.FechaC=this.formulario.get('FechaC').value
            this.Datos.FechaE=this.formulario.get('FechaE').value
            this.Datos.Horas=this.formulario.get('Horas').value
            this._serviceM.agregarConstalaci(this.Datos).subscribe((data:any)=>{
              if(data.ok){
                alert("Registro agregado correctamente");
                console.log(this.Datos);
              }
            })
        }
      }
      else {
        alert("Ingresa los datos faltantes");
      } 
}

 imprimir(){ 
/* if (this.formulario.valid) {*/
    
    this.NombreP=this.formulario.get('NombreP').value
    this.Clave=this.formulario.get('clave').value
    this.FechaI = new Date(this.formulario.get('FechaI').value).toLocaleDateString('es-ES');
    this.FechaC = new Date(this.formulario.get('FechaC').value).toLocaleDateString('es-ES');

    /*this.FechaI=this.formulario.get('FechaI').value
    this.FechaC=this.formulario.get('FechaC').value*/
    this.FechaE=this.formulario.get('FechaE').value
    this.Horas=this.formulario.get('Horas').value

    for (let index = 0; index < this.experienciaLaboral.length; index++) { 
    this.Nombre=this.experienciaLaboral.value[index]['Nombre'];
    this.Rol=this.experienciaLaboral.value[index]['Rol'];
    var conta = 0;   
    var doc = new jsPDF()
    var image1 = new Image();
    image1.src = "assets/membrete.png"; /// URL de la imagen
    doc.addImage(image1, 'PNG',2, 53, 56, 200);

    var image1 = new Image();
    image1.src = "assets/pie.png"; /// URL de la imagen
    doc.addImage(image1, 'PNG',20, 268, 165, 27); // Agregar la imagen al PDF (X, Y, Width, Height)
    var image2 = new Image();
    image2.src = "assets/encabezado.png"; 
    doc.addImage(image2, 'PNG',35, 10, 130, 17); 
    
    doc.setFontSize(20);
    doc.setFont("Helvetica", "bold");
    doc.text("EL TECNOLÓGICO NACIONAL DE MÉXICO", 36, 45);

    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.text("A TRAVÉS DEL INSTITUTO TECNOLÓGICO DE CUAUTLA", 40, 55);
    
    doc.setFont("Helvetica", "normal");
    doc.text("OTORGA LA PRESENTE", 76, 70);

    doc.setFontSize(22);
    doc.setFont("Helvetica", "bold");
    doc.text("CONSTANCIA", 78, 90);

    let text="A"
    let pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    let textWidth = doc.getStringUnitWidth(text) * 15 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    let x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto

    doc.setFont("Helvetica", "normal");
    doc.text(text, 103, 105);

    const texto = this.Nombre;
    const fontSize = 25;
    const fontType = "bold";
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(texto) * fontSize / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto

    doc.setFontSize(fontSize);
    doc.setFont("Helvetica", fontType);
    doc.text(texto, x, 120);


    doc.setFontSize(12);
    doc.setFont("Helvetica", "normal");
    doc.text("POR SU VALIOSA PARTICIPACIÓN EN:",64, 133);
    

    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(this.NombreP) * 16 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFontSize(16);
    doc.setFont("Helvetica", "bold");
    doc.text(this.NombreP,x, 143);

    let texto2="Con clave"
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(texto2) * 13 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFont("Helvetica", "normal");
    doc.text(texto2,x, 152);

    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(this.Clave) * 16 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFontSize(16);
    doc.setFont("Helvetica", "bold");
    doc.text(this.Clave,x, 162);


    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.text(`Fecha inicio: ${this.FechaI}`,45, 174);
    doc.setFontSize(13);
    
    doc.setFont("Helvetica", "bold");
    doc.text(`Fecha Finalización: ${this.FechaC}`,104, 174);

    let fechaEn=`H.H. Cuautla, Mor. a ${this.FechaE}`
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(fechaEn) * 13 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2;
    doc.setFont("Helvetica", "normal");
    doc.text(fechaEn,x, 185);
      
    var firmas = new Image();
    firmas.src = "assets/firma.png"; 
    doc.addImage(firmas, 'PNG',90, 195, 30, 30); 

    doc.setFont("Helvetica", "bold");
    doc.text("M.C. PORFIRIO ROBERTO NÁJERA MEDINA ", 57, 230);
    doc.setFont("Helvetica", "bold");
    doc.text("DIRECTOR DEL INSTITUTO TECNOLÓGICO DE CUAUTLA", 40, 240);

    

    doc.setTextColor(150);
    doc.save('CONSTANCIA.pdf')
  }
 }
 

 
 /*
 else{
  alert("Ingresa los datos faltantes");
 }
}*/
}
