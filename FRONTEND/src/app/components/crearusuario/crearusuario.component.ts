import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-crearusuario',
  templateUrl: './crearusuario.component.html',
  styleUrls: ['./crearusuario.component.css']
})
export class CrearusuarioComponent implements OnInit {
  hide = true;
  Datos={nombreUsuario:'', email:'', password:'', role:''}

  constructor( private _serviceM:ApiService) { }

  ngOnInit(): void {
  }

  AgregarUser(){  
    if(this.Datos.nombreUsuario=='' || this.Datos.email=='' || this.Datos.password=='' || this.Datos.role==''){
      window.alert("Debes ingresar todos los datos solicitados")
    }  
    else{
      console.log(this.Datos);
    this._serviceM.agregarUser(this.Datos).subscribe((data:any)=>{
      if (data ['ok']) {
        alert("Registrado exitosamente");
      } else   
      alert(data.error);
    })}
  }
}
