import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecocimientoComponent } from './recocimiento.component';

describe('RecocimientoComponent', () => {
  let component: RecocimientoComponent;
  let fixture: ComponentFixture<RecocimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecocimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecocimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
