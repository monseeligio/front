import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaRComponent } from './consulta-r.component';

describe('ConsultaRComponent', () => {
  let component: ConsultaRComponent;
  let fixture: ComponentFixture<ConsultaRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
