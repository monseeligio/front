import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import jsPDF from 'jspdf';
import * as moment from 'moment';


@Component({
  selector: 'app-consulta-r',
  templateUrl: './consulta-r.component.html',
  styleUrls: ['./consulta-r.component.css']
})
export class ConsultaRComponent implements OnInit {
  public datos2:any[];
  id='';
  Nombre='';
  Rol='';
  NombreP:'';
  Clave='';
  FechaI='';
  FechaC='';
  FechaE='';
  Horas='';
  constructor(private _serviceM:ApiService) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    this._serviceM.consultareco().subscribe((resp:any)=>{
      if (resp['ok']){
        console.log(resp);
        this.datos2=resp['respuesta'];
        console.log(this.datos2);
      }
    })
  }
  
  
  Eliminar(item:any){    
    let id2=item._id
    this._serviceM.eliminarReconocimiento(id2).subscribe((data:any)=>{
      console.log(this.id);
      alert("Dato eliminado correctamente");
      this.getUser()
    })
  }

  manejarClicFila(item: any) {
    // hacer algo con los datos de la fila, por ejemplo:
    console.log(`Se hizo clic en la fila de ${item.Nombre}`);
    let nombre=item.Nombre
    let rol=item.Rol
    let nombrep=item.NombreP
    let clave=item.Clave
    let fechai=item.FechaI
    let fechaC=item.FechaC
    let fechaE= item.FechaE
    let horas= item.Horas
    const fechaFormateada = moment.utc(fechai).format("DD/MM/YYYY");
    const fechaFormateada2 = moment.utc(fechaC).format("DD/MM/YYYY");


    var conta = 0;   
    var doc = new jsPDF()
    var image1 = new Image();
    image1.src = "assets/membrete.png"; /// URL de la imagen
    doc.addImage(image1, 'PNG',2, 53, 56, 200);

    var image1 = new Image();
    image1.src = "assets/pie.png"; /// URL de la imagen
    doc.addImage(image1, 'PNG',20, 268, 165, 27); // Agregar la imagen al PDF (X, Y, Width, Height)
    var image2 = new Image();
    image2.src = "assets/encabezado.png"; 
    doc.addImage(image2, 'PNG',35, 10, 130, 17); 
    
    doc.setFontSize(20);
    doc.setFont("Helvetica", "bold");
    doc.text("EL TECNOLÓGICO NACIONAL DE MÉXICO", 36, 45);

    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.text("A TRAVÉS DEL INSTITUTO TECNOLÓGICO DE CUAUTLA", 40, 55);
    
    doc.setFont("Helvetica", "normal");
    doc.text("OTORGA EL PRESENTE", 76, 70);

    doc.setFontSize(22);
    doc.setFont("Helvetica", "bold");
    doc.text("RECONOCIMIENTO", 70, 90);

    

    let text="A"
    let pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    let textWidth = doc.getStringUnitWidth(text) * 15 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    let x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto

    doc.setFont("Helvetica", "normal");
    doc.text(text, 103, 105);

    const texto = nombre;
    const fontSize = 25;
    const fontType = "bold";
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(texto) * fontSize / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto

    doc.setFontSize(fontSize);
    doc.setFont("Helvetica", fontType);
    doc.text(texto, x, 120);


    doc.setFontSize(12);
    doc.setFont("Helvetica", "normal");
    doc.text("POR SU VALIOSA PARTICIPACIÓN EN:",64, 133);
    

    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(nombrep) * 16 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFontSize(16);
    doc.setFont("Helvetica", "bold");
    doc.text(nombrep,x, 143);

    let texto2="Con clave"
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(texto2) * 13 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFont("Helvetica", "normal");
    doc.text(texto2,x, 152);

    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(clave) * 16 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2; // Calcular la coordenada X para centrar el texto
    doc.setFontSize(16);
    doc.setFont("Helvetica", "bold");
    doc.text(clave,x, 162);


    doc.setFontSize(13);
    doc.setFont("Helvetica", "bold");
    doc.text(`Fecha inicio: ${fechaFormateada}`,45, 174);
    doc.setFontSize(13);
    
    doc.setFont("Helvetica", "bold");
    doc.text(`Fecha Finalización: ${fechaFormateada2}`,104, 174);

    let fechaEn=`H.H. Cuautla, Mor. a ${fechaE}`
    pageWidth = doc.internal.pageSize.width; // Ancho de la página en unidades de PDF
    textWidth = doc.getStringUnitWidth(fechaEn) * 13 / doc.internal.scaleFactor; // Ancho del texto en unidades de medida
    x = (pageWidth - textWidth) / 2;
    doc.setFont("Helvetica", "normal");
    doc.text(fechaEn,x, 185);
      
    var firmas = new Image();
    firmas.src = "assets/firma.png"; 
    doc.addImage(firmas, 'PNG',90, 195, 30, 30); 

    doc.setFont("Helvetica", "bold");
    doc.text("M.C. PORFIRIO ROBERTO NÁJERA MEDINA ", 57, 230);
    doc.setFont("Helvetica", "bold");
    doc.text("DIRECTOR DEL INSTITUTO TECNOLÓGICO DE CUAUTLA", 40, 240);

    

    doc.setTextColor(150);
    doc.save('RECONOCIMIENTO.pdf')
  }
  

}

 


