import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import {MatStepperModule } from '@angular/material/stepper';
import { MatExpansionModule} from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import {ROUTES} from './app.route';
import { ConsultarComponent } from './components/consultar/consultar.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RecocimientoComponent } from './components/recocimiento/recocimiento.component';
import { ConstanciaComponent } from './components/constancia/constancia.component';
import { FormsModule } from '@angular/forms';
import { CrearusuarioComponent } from './components/crearusuario/crearusuario.component';
//api material angular
import {MatButtonModule} from '@angular/material/button';

import { LoginComponent } from './components/login/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';

import {MatInputModule} from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { EditarusuarioComponent } from './components/editarusuario/editarusuario.component';
import {MatDialogModule} from '@angular/material/dialog';
import {HttpClientModule } from '@angular/common/http';
import { MenuNormalComponent } from './components/menu-normal/menu-normal.component'
import { NgxQRCodeModule } from "@techiediaries/ngx-qrcode";
import {NgxPrintModule} from 'ngx-print';
import { ConsultaRComponent } from './components/consulta-r/consulta-r.component';


//import { QRCodeModule } from 'angularx-qrcode';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ConsultarComponent,
    UsuariosComponent,
    RecocimientoComponent,
    ConstanciaComponent,
    LoginComponent,
    CrearusuarioComponent,
    EditarusuarioComponent,
    MenuNormalComponent,
    ConsultaRComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatStepperModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES, {useHash:true}),
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    MatSelectModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxQRCodeModule,
    NgxPrintModule
       
    

    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
